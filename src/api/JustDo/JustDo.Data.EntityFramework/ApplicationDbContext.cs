﻿using JustDo.Data.Entities;
using JustDo.Data.EntityFramework.Database;
using Microsoft.EntityFrameworkCore;

namespace JustDo.Data.EntityFramework
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        { }

        public DbSet<Goal> Goals { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {            
            builder.RemovePluralizingTableNameConvention();

            base.OnModelCreating(builder);
            ConfigureRelationships(builder); // if we will use 
        }

        private void ConfigureRelationships(ModelBuilder builder)
        {
            var goalBuilder = builder.Entity<Goal>();
          //  goalBuilder.Property(x => x.DueDate).HasColumnType("")
            goalBuilder.Property(x => x.IsFinished).HasDefaultValue(false);
            goalBuilder.HasIndex(x => x.IsFinished);
            goalBuilder.HasIndex(x => x.DueDate); // todo: try to add desc sorting (not supported with current provider)
        }
    }
}
