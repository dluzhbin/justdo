﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JustDo.Core.Models.Response
{
    public class ErrorResponseModel
    {
        public ErrorResponseModel(IEnumerable<string> messages)
            : this(messages.ToArray())
        {
        }

        public ErrorResponseModel(params string[] messages)
        {
            Messages = messages;
            Date = DateTime.Now;
        }

        public IReadOnlyList<string> Messages { get; }

        public DateTime Date { get; }
    }
}
