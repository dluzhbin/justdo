﻿using JustDo.Data.BaseModels;
using JustDo.Data.Entities;
using System;
using System.Linq.Expressions;

namespace JustDo.Core.Models.Response
{
    public class GoalResponseModel : GoalModel
    {
        public static Expression<Func<Goal, GoalResponseModel>> Projection
        {
            get
            {
                return x => new GoalResponseModel()
                {
                    Id = x.Id,
                    Title = x.Title,
                    Description = x.Description,
                    DueDate = x.DueDate,
                    RemindMinutes = x.RemindMinutes,
                    Priority = x.Priority
                };
            }
        }
    }
}
