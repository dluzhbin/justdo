﻿using System;

namespace JustDo.Core.Models.Response
{
    /// <remarks>
    /// it will be better for performance, because 
    /// ordering and filtering arrays slower at client side then server side
    /// </remarks>
    public class GoalDayResponseModel
    {
        public DateTime Date { get; set; }
        public GoalResponseModel[] Goals { get; set; }
    }
}
