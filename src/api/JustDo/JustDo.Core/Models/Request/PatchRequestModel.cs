﻿namespace JustDo.Core.Models.Request
{
    public class PatchRequestModel
    {
        public string FieldName { get; set; }
        public object Value { get; set; }
    }
}
