﻿using JustDo.Data.BaseModels;

namespace JustDo.Core.Models.Request
{
    public class GoalRequestModel : GoalModel
    {
        public int NotificationSlideMinutes { get; set; }

        public bool IsFinished { get; set; }
    }
}
