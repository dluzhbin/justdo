﻿using JustDo.Core.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace JustDo.Core.Models.Request
{
    public class GoalAddRequestModel
    {
        [Required]
        [MaxLength(256)]
        public string Title { get; set; }

        [MaxLength(4000)]
        public string Description { get; set; }

        [Required]
        public int RemindMinutes { get; set; }

        [Required]
        public DateTimeOffset DueDate { get; set; }

        public TaskPriority Priority { get; set; }
    }
}
