﻿using AutoMapper;
using JustDo.Core.Models.Request;
using JustDo.Core.Models.Response;
using JustDo.Data.BaseModels;
using JustDo.Data.Entities;

namespace JustDo.Core.Mappings
{
    public class GoalMapping : Profile
    {
        public GoalMapping()
        {
            CreateMap<Goal, GoalResponseModel>(MemberList.Destination);
            CreateMap<GoalAddRequestModel, Goal>();
         //   CreateMap<GoalAddRequestModel, Goal>();
        }
    }
}
