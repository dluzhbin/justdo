﻿namespace JustDo.Core.Exceptions
{
    using System;
    using System.Net;

    public class BadRequestException : Exception
    {
        public BadRequestException(string message)
            : base(message)
        {
            StatusCode = (int)HttpStatusCode.BadRequest;
        }

        public int StatusCode { get; }
    }
}
