﻿namespace JustDo.Core.Exceptions
{
    using System;
    using System.Net;

    public class NotFoundException : Exception
    {
        public NotFoundException(string message)
            : base(message)
        {
            StatusCode = (int)HttpStatusCode.NotFound;
        }

        public int StatusCode { get; }
    }
}
