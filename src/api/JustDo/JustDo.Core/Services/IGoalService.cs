﻿using JustDo.Core.Models.Request;
using JustDo.Core.Models.Response;
using System.Threading.Tasks;

namespace JustDo.Core.Services
{
    public interface IGoalService
    {
        Task<GoalDayResponseModel[]> Get(long userId);
        Task<GoalResponseModel> Get(long id, long userId);
        GoalResponseModel Add(GoalAddRequestModel model, long userId);
        void Update(long id, PatchRequestModel model, long userId);
        void Delete(long id, long userId);
    }
}
