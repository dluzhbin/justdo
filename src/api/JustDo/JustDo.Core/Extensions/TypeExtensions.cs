﻿namespace JustDo.Core.Extensions
{
    public static class TypeExtensions
    {
        public static bool HasProperty(this object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName) != null;
        }
    }
}
