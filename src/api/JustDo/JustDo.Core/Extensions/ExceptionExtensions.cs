﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JustDo.Core.Extensions
{
    public static class ExceptionExtensions
    {
        public static IEnumerable<string> GetAllMessages(this Exception ex)
        {
            return ex.GetInnerExceptions().Select(x => x.Message);
        }

        public static IEnumerable<Exception> GetInnerExceptions(this Exception ex)
        {
            if (ex == null)
            {
                throw new ArgumentNullException("ex");
            }

            var innerException = ex;
            do
            {
                yield return innerException;
                innerException = innerException.InnerException;
            }
            while (innerException != null);
        }
    }
}
