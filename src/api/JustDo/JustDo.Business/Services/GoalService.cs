﻿using AutoMapper;
using JustDo.Core.Exceptions;
using JustDo.Core.Models.Request;
using JustDo.Core.Models.Response;
using JustDo.Core.Services;
using JustDo.Data.Entities;
using JustDo.Data.EntityFramework;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace JustDo.Business.Services
{
    public class GoalService : IGoalService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private const string GOAL_NOT_FOUND = "Goal not found"; // must stored at resources file
        private const string PROPERTY_NOT_FOUND = "Goal has no target property"; // must stored at resources file


        public GoalService(ApplicationDbContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public async Task<GoalDayResponseModel[]> Get(long userId)
        {
            // just get fields what we needed (not full entity). we can execute query immediately because we can group it at .net side faster
            var goals = await GetActiveGoals(userId).ToArrayAsync();

            var result = (from g in goals
                          group g by g.DueDate.Date into gr
                          select new GoalDayResponseModel()
                          {
                              Date = gr.Key,
                              Goals = gr.ToArray()
                          }).OrderBy(x => x.Date).ToArray();
            
            return result;
        }

        public async Task<GoalResponseModel> Get(long id, long userId)
        {
            /* note: we can implement one of two strategies if the creator and current user are not the same
             * Forbidden or Not found. I picked a 2nd. 
             * */
            var goal = await GetActiveGoals(userId).FirstOrDefaultAsync(x => x.Id == id);
            if (goal == null)
                throw new NotFoundException(GOAL_NOT_FOUND);

            return goal;
        }

        public GoalResponseModel Add(GoalAddRequestModel model, long userId)
        {
            // todo: validation
            var goal = _mapper.Map<Goal>(model);
            goal.UserId = userId;
            goal.NotificationDate = goal.DueDate.AddMinutes(-goal.RemindMinutes);
            _context.Add(goal);
            _context.SaveChanges();

            return _mapper.Map<GoalResponseModel>(goal);
        }

        public void Update(long id, PatchRequestModel model, long userId)
        {
            // todo: validation
            var goal = _context.Goals.AsNoTracking().Where(x => x.Id == id && x.UserId == userId)
                .Select(x => new Goal() { Id = x.Id }).FirstOrDefault();
            if (goal == null)
                throw new NotFoundException(GOAL_NOT_FOUND);

            var property = typeof(Goal).GetProperty(model.FieldName);
            if (property == null)
                throw new BadRequestException(PROPERTY_NOT_FOUND);
            
            var value = Convert.ChangeType(model.Value, property.PropertyType);

            property.SetValue(goal, value);
            _context.Entry(goal).Property(model.FieldName).IsModified = true;
            _context.SaveChanges();
        }

        public void Delete(long id, long userId)
        {
            // todo: validation
            var goal = _context.Goals.AsNoTracking().Where(x => x.Id == id && x.UserId == userId)
                .Select(x => new Goal() { Id = x.Id, IsFinished = x.IsFinished }).FirstOrDefault();
            if (goal.IsFinished) return;
            
            goal.IsFinished = true;
            
            _context.Entry(goal).Property(x => x.IsFinished).IsModified = true;
            _context.SaveChanges();
        }

        private IQueryable<GoalResponseModel> GetActiveGoals(long userId)
        {
            return _context.Goals.AsNoTracking().Where(x => x.UserId == userId && !x.IsFinished).Select(GoalResponseModel.Projection);
        }
    }
}
