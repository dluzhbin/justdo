﻿using JustDo.Data.BaseModels;
using System;

namespace JustDo.Data.Entities
{
    /// <summary>
    /// Goal
    /// </summary>
    /// <remarks>
    /// better name will be Task, but we have a class named Task at .Net and it can confuse sometimes)
    /// </remarks>
    /// <seealso cref="JustDo.Data.BaseModels.GoalModel" />
    public class Goal : GoalModel
    {
        public long UserId { get; set; }

        /// <summary>
        /// Gets or sets the notification date.
        /// </summary>
        /// <value>
        /// The notification date.
        /// </value>
        /// <remarks>
        /// concrete date for improve performance and for a case when a user can 
        /// postpone notification for any time.
        /// We shouldn't create a separate table because as I think we shouldn't schedule
        /// a lot of notification for a single task
        /// </remarks>
        public DateTime NotificationDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is finished.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is finished; otherwise, <c>false</c>.
        /// </value>
        /// <remarks>
        /// don't create separate field for an IsDeleted flag 
        /// because we have no functionality for viewing finished tasks, so KISS in an action
        /// </remarks>
        public bool IsFinished { get; set; }
    }
}
