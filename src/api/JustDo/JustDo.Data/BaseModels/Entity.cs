﻿namespace JustDo.Data.BaseModels
{
    public class Entity<T>
    {
        public T Id { get; set; }
    }
}
