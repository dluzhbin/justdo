﻿using JustDo.Core.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace JustDo.Data.BaseModels
{
    public class GoalModel : Entity<long>
    {
        [Required]
        [MaxLength(256)]
        public string Title { get; set; }
        
        [MaxLength(4000)]
        public string Description { get; set; }

        public int RemindMinutes { get; set; }

        public DateTime DueDate { get; set; }
        
        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        /// <value>
        /// The priority.
        /// </value>
        /// <remarks>
        /// have no reasons for change list of priority types, so without a separate table
        /// </remarks>
        public TaskPriority Priority { get; set; }
    }
}
