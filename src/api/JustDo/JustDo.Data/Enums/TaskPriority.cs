﻿namespace JustDo.Core.Models.Enums
{
    public enum TaskPriority
    {
        Neutral = 0,
        Low = 0,
        Important = 1,
        Urgent = 2
    }
}
