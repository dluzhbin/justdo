﻿using JustDo.Business.Services;
using JustDo.Core.Services;
using JustDo.Core.Settings;
using JustDo.Data.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;

namespace JustDo.Configurations
{
    /// <summary>
    /// Dependencies configuration
    /// </summary>
    public static class DependenciesConfiguration
    {
        /// <summary>
        /// Adds the database context.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="configuration">The configuration.</param>
        public static void AddDbContext(this IServiceCollection services, IConfigurationRoot configuration)
        {
            var dbSettings = configuration.GetSection("Database")
                                          .Get<DatabaseSettings>();
            services
                .AddDbContext<ApplicationDbContext>(opts => opts.UseNpgsql(dbSettings.ConnectionString, it => it.MigrationsHistoryTable("__EFMigrationsHistory", "public")))
                .AddEntityFrameworkSqlServer();
        }

        /// <summary>
        /// Adds the swagger.
        /// </summary>
        /// <param name="services">The services.</param>
        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(setup =>
            {
                setup.SwaggerDoc("v1", new Info { Title = "JustDo", Version = "v1" });
                setup.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "JustDo.Documentation.xml"));
/*
                setup.AddSecurityDefinition("Bearer", new ApiKeyScheme { In = "header", Description = "Enter 'Bearer {token}' (don't forget to add 'bearer') into the field below.", Name = "Authorization", Type = "apiKey" });

                setup.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer", Enumerable.Empty<string>() },
                });
                */
                //setup.OperationFilter<OptionOperationFilter>();
                setup.DescribeAllEnumsAsStrings();
            });
        }

        /// <summary>
        /// Adds the services.
        /// </summary>
        /// <param name="services">The services.</param>
        public static void AddServices(this IServiceCollection services)
        {
            services.Scan(scan =>
            {
                scan.FromAssemblyOf<GoalService>()
                    .AddClasses(classes => classes.InNamespaces("JustDo.Business.Services"))
                    .AsMatchingInterface()
                    .WithTransientLifetime();
            });
        }
    }
}
