﻿using Microsoft.AspNetCore.Builder;

namespace JustDo.Configurations
{
    /// <summary>
    /// Middleware configuration
    /// </summary>
    public static class MiddlewareConfiguration
    {
        /// <summary>
        /// Uses the swagger.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="endpointName">Name of the endpoint.</param>
        public static void UseSwagger(this IApplicationBuilder app, string endpointName)
        {
            app.UseSwagger();
            app.UseSwaggerUI(setup =>
            {
                setup.RoutePrefix = string.Empty;

                setup.SwaggerEndpoint(
                    url: "/swagger/v1/swagger.json",
                    name: endpointName);
            });
        }
    }
}
