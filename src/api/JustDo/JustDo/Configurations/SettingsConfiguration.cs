﻿using JustDo.Core.Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace JustDo.Configurations
{
    /// <summary>
    /// Settings configuration
    /// </summary>
    public static class SettingsConfiguration
    {
        /// <summary>
        /// Adds the settings.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="configuration">The configuration.</param>
        public static void AddSettings(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.Configure<DatabaseSettings>(configuration.GetSection("Database"));
        }
    }
}
