﻿using JustDo.Core.Exceptions;
using JustDo.Core.Extensions;
using JustDo.Core.Models.Response;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;

namespace JustDo.Filters
{
    /// <summary>
    /// Exception filter
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Filters.IExceptionFilter" />
    public class ExceptionFilter : IExceptionFilter
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public object Log { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionFilter"/> class.
        /// </summary>
        /// <param name="environment">The environment.</param>
        public ExceptionFilter(IHostingEnvironment environment)
        {
            _hostingEnvironment = environment;
        }

        /// <summary>
        /// Called after an action has thrown an <see cref="T:System.Exception" />.
        /// </summary>
        /// <param name="context">The <see cref="T:Microsoft.AspNetCore.Mvc.Filters.ExceptionContext" />.</param>
        public void OnException(ExceptionContext context)
        {
            var status = GetStatusCode(context);
            context.HttpContext.Response.StatusCode = status;

            IActionResult result = _hostingEnvironment.IsDevelopment() ?
                GetDevelopException(context) :
                GetProductionException(context);

            context.Result = result;
            LogException(context);
        }

        private void LogException(ExceptionContext context)
        {
            if (context.HttpContext.Response.StatusCode == (int)HttpStatusCode.InternalServerError)
            {
                // todo: log error
            }
        }

        private JsonResult GetDevelopException(ExceptionContext context)
        {
            var error = new ErrorResponseModel(context.Exception.GetAllMessages());
            return new JsonResult(error);
        }

        private JsonResult GetProductionException(ExceptionContext context)
        {
            if (context.HttpContext.Response.StatusCode == (int)HttpStatusCode.InternalServerError)
            {
                return new JsonResult(new ErrorResponseModel("An unexpected internal server error has occurred."));
            }

            return new JsonResult(new ErrorResponseModel(context.Exception.GetAllMessages()));
        }

        private int GetStatusCode(ExceptionContext context)
        {
            var status = (int)HttpStatusCode.InternalServerError;

            if (context.Exception is BadRequestException badRequestException)
            {
                status = badRequestException.StatusCode;
            }

            if (context.Exception is NotFoundException notFoundException)
            {
                status = notFoundException.StatusCode;
            }

            return status;
        }
    }
}