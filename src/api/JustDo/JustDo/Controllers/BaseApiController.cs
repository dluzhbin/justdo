﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace JustDo.Controllers
{
    /// <summary>
    /// Base API controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("[controller]/[action]")]
    [ApiController]
    public class BaseApiController : ControllerBase
    {
        /// <summary>
        /// Gets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public long? UserId =>
               long.TryParse(User.FindFirstValue("sub"), out var i) ? i : (long?)1 /* for testing before registration will be implemented*/;

        /// <summary>
        /// Gets the created URL.
        /// </summary>
        /// <param name="routeName">Name of the route.</param>
        /// <param name="opts">The opts.</param>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        public string CreatedUrl(string routeName, object opts)
        {
            return Url.RouteUrl(routeName, opts, Request.Scheme, Request.Host.ToUriComponent());
        }
    }
}