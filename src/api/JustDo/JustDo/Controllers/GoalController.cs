﻿using JustDo.Core.Models.Request;
using JustDo.Core.Models.Response;
using JustDo.Core.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace JustDo.Controllers
{
    //[Authorize]
    /// <summary>
    /// Goal controller
    /// </summary>
    /// <seealso cref="JustDo.Controllers.BaseApiController" />
    [Route("goals")]
    public class GoalController : BaseApiController
    {
        private readonly IGoalService _goalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GoalController"/> class.
        /// </summary>
        /// <param name="goalService">The goal service.</param>
        public GoalController(IGoalService goalService)
        {
            this._goalService = goalService;
        }

        /// <summary>
        /// Gets list of goals.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(GoalDayResponseModel[]), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetList()
        {
            // note: better way - using pagination for all request with list of entity
            // with standart not mandatory fields: searchTerm, limit/offset, order with direction
            return Ok(await _goalService.Get(UserId.Value));
        }

        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id:long}", Name = "GetGoal")]
        [ProducesResponseType(typeof(GoalDayResponseModel[]), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(long id)
        {
            return Ok(await _goalService.Get(id, UserId.Value));
        }

        /// <summary>
        /// Adds the specified request.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(GoalResponseModel), StatusCodes.Status201Created)]
        public IActionResult Add(GoalAddRequestModel model)
        {
            var goal = _goalService.Add(model, UserId.Value);

            return Created(CreatedUrl("GetGoal", new { id = goal.Id }), goal);
        }

        /// <summary>
        /// Updates the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPatch]
        [Route("{id:long}")]
        [ProducesResponseType(typeof(GoalDayResponseModel[]), StatusCodes.Status200OK)]
        public IActionResult Update(long id, PatchRequestModel model)
        {
            _goalService.Update(id, model, UserId.Value);
            return Ok();
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id:long}")]
        [ProducesResponseType(typeof(GoalDayResponseModel[]), StatusCodes.Status200OK)]
        public IActionResult Delete(long id)
        {
            _goalService.Delete(id, UserId.Value);
            return Ok();
        }
    }
}